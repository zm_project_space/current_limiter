package com.example.current_limiter.exception;

/**
 * className: LimitException.java
 * author: zhang meng
 * description:
 * date: 2020/6/24 17:49
 */
public class LimitException extends Exception {


    long code = -1l; //默认为-1 未知错误

    public LimitException() {
    }

    public long getCode() {
        return code;
    }

    public LimitException(String message) {
        super(message);
    }

    public LimitException(long code, String message) {
        super(message);
        this.code = code;
    }

    public LimitException(String message, Throwable cause) {
        super(message, cause);
    }

    public LimitException(long code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public LimitException(Throwable cause) {
        super(cause);
    }

    public LimitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
