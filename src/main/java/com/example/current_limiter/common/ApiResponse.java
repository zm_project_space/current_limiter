package com.example.current_limiter.common;

import com.example.current_limiter.exception.LimitException;

import java.io.Serializable;

/**
 * className: ApiResponse.java
 * author: zhang meng
 * description: Api 统一返回
 * date: 2020/6/24 16:45
 */
public class ApiResponse<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int SUCCESS = 0;
    public static final int FAIL = -1;
    public static final int BUSY = -100;
    public static final String SUCCESS_TEXT = "Success";
    public static final String FAIL_TEXT = "Fail";
    public static final String BUSY_TEXT = "Busy";
    private long status;
    private String statusText;
    private T data;

    public ApiResponse() {
        this(0L, "Success", null);
    }

    public ApiResponse(long status, String statusText) {
        this(status, statusText, null);
    }

    public ApiResponse(long status, String statusText, T data) {
        this.status = status;
        this.statusText = statusText;
        this.data = data;
    }

    public ApiResponse(T data) {
        if (data instanceof LimitException) {
            LimitException ex = (LimitException) data;
            this.status = ex.getCode();
            this.statusText = ex.getLocalizedMessage();
        } else if (data instanceof Exception) {
            Exception ex = (Exception) data;
            this.status = -1L;
            this.statusText = ex.getLocalizedMessage();
        } else {
            this.status = 0L;
            this.statusText = "Success";
            this.data = data;
        }

    }

    public long getStatus() {
        return this.status;
    }

    public void setStatus(long status) {
        this.status = status;
        if (status == 0L) {
            this.statusText = "Success";
        } else if (status == -100L) {
            this.statusText = "Busy";
        } else {
            this.statusText = "";
        }

    }

    public String getStatusText() {
        return this.statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <M> ApiResponse<M> ok() {
        return new ApiResponse();
    }

    public static <T> ApiResponse<T> ok(T data) {
        return new ApiResponse(data);
    }
}
