package com.example.current_limiter.common;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * className: ControllerException.java
 * author: zhang meng
 * description: 处理未捕获的异常
 * date: 2020/6/24 17:13
 */

@ControllerAdvice
@ResponseBody
public class ControllerException {

    @ExceptionHandler(value = Exception.class)
    public ApiResponse exception(Exception e) {
        return new ApiResponse(e);
    }
}
