package com.example.current_limiter.common;

/**
 * className: LimitType.java
 * author: zhang meng
 * description: 限流枚举类
 * date: 2020/6/24 14:05
 */
public enum LimitType {

    /**
     * 自定义 key
     */
    CUSTOMER,

    /**
     * 请求者 IP
     */
    IP
}
