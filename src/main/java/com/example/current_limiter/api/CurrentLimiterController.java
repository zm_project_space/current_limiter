package com.example.current_limiter.api;

import com.example.current_limiter.annotation.Limit;
import com.example.current_limiter.common.ApiResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * className: CurrentLimiterController.java
 * author: zhang meng
 * description:
 * date: 2020/6/24 15:36
 */
@RestController
@RequestMapping("/api/current/limit")
public class CurrentLimiterController {

    private static final AtomicInteger ATOMIC_INTEGER = new AtomicInteger();

    private static final AtomicInteger ATOMIC_INTEGER_2 = new AtomicInteger();

    /**
     * 10秒内只允许访问3次，为了直观一点，这里使用 AtomicInteger 计数
     */
    @GetMapping("/test")
    @Limit(key = "current_limit", period = 10, count = 3)
    public ApiResponse test() {
        return new ApiResponse(ATOMIC_INTEGER.incrementAndGet());
    }

    @GetMapping("/test1")
    public ApiResponse test1() {
        return new ApiResponse("访问成功，当前访问量：" +  ATOMIC_INTEGER_2.incrementAndGet());
    }
}
